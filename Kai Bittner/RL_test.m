x0 = [100; 100; 1];
C = [0 0 0; 0 0 0; 0 0 0.01];
G = [0 0 1; 0 0.01 -1; -1 1 0];
dt = 1.1e-6;
t = 0:dt:4e-4;
s = [-1; 0; 0] * max (min (2-10e3*t,1), 0) ;
% s = [-1; 0; 0] * abs(cos(1e3*pi*t))
% s = [-1; 0; 0] * abs(cos(2e3*pi*t)).*cos(2e3*pi*t)

len = size(s,2);
% start = round(1/dt)+1;
start = 1;

x = trapez_lin (6, dt, x0, C, G, s);

figure(1)
hold off; plot(t(start:len),x(1,start:len)); hold on; plot(t(start:len),x(2,start:len),'k'); plot(t(start:len),x(3,start:len),'r'); 
legend('u_1(t)','u_2(t)','I(t)');
title('Trapezoidal rule');

order = 2;
x = bdf_lin (order, 3, dt, x0, C, G, s);

figure(2)
hold off; plot(t(start:len),x(1,start:len)); hold on; plot(t(start:len),x(2,start:len),'k'); 
legend('u_1(t)','u_2(t)','I(t)');
title('BDF');

% x = trap_bdf_lin (2, 3, dt, x0, C, G, s, 10);
% 
% figure(3)
% hold off; plot(t(start:len),x(1,start:len)); hold on; plot(t(start:len),x(2,start:len),'k'); 
% legend('u_1(t)','u_2(t)','I(t)');
% title('TRAP_BDF');
