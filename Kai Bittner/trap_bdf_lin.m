function x = trap_bdf_lin(order, n, dt, x0, C, G, s, fac)
% Trapezoidal rule for linear DAE  C x'(t) + G x(t) + s(t) = 0
% Input
%  order    : BDF order
%  n        : Number of Unknowns
%  dt       : stepsize 
%  x0(n)    : Initial value x0 = x(0)
%  C(n,n)   : Matrix C
%  G(n,n)   : Matrix G
%  s(n,len) : source Term
% Output
%  x        : solution

a = [    1 -1    0     0    0    0   0;
       3/2 -2  1/2     0    0    0   0;
      11/6 -3  3/2  -1/3    0    0   0;
     25/12 -4    3  -4/3  1/4    0   0;
    137/60 -5    5 -10/3  5/4 -1/5   0;
     49/20 -6 15/2 -20/3 15/4 -6/5 1/6];

len = size(s,2);
x   = x0;
w   = dt / 2;
order = min(order, 6);
for k = 2:len
    if mod (k,fac) == 0
       m = min(k-1,order); % adapt order to availble data
       rhs = - C * x(:,k-1:-1:k-m) * a(m,2:m+1)' - dt * s(:,k);  
       mat = a(m,1) * C + dt * G;
    else
       rhs = (C - w * G) * x(:,k-1) - w * (s(:,k) + s(:,k-1));
       mat = C + w * G;
    end
    x = cat (2, x, mat\rhs);
end
end
