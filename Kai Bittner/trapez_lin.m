function x = trapez_lin(n, dt, x0, C, G, s)
% Trapezoidal rule for linear DAE  C x'(t) + G x(t) + s(t) = 0
% Input
%  n        : Number of Unknowns
%  dt       : stepsize 
%  x0(n)    : Initial value x0 = x(0)
%  C(n,n)   : Matrix C
%  G(n,n)   : Matrix G
%  s(n,len) : source Term
% Output
%  x        : solution

len = size(s,2);
x   = x0;
w   = dt / 2;
for k = 2:len
    rhs = (C - w * G) * x(:,k-1) - w * (s(:,k) + s(:,k-1));
    mat = C + w * G;
    x = cat (2, x, mat\rhs);
end
end
