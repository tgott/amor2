addpath rfsoc_matlab
clc
clearvars
close all

N=8192; % number of samples to capture
ip='192.168.0.3'; % ip address of the ZCU111 board
session=open_rfsoc(ip); % open the session
%%
load OFDM_shifted.mat
figure;
plot(20*log10(abs(fftshift(fft(data_tx))))); % plot the TX data

send_and_wait_for_response(session.command_socket, 'LocalMemTrigger 1 0 0 0x0000'); % reset DAC DMA
write_data(session, data_tx); % send data to DAC buffer
send_and_wait_for_response(session.command_socket, 'LocalMemTrigger 1 1 0 0x0040'); % start DAC DMA
%%
send_and_wait_for_response(session.command_socket, 'LocalMemTrigger 0 0 4096 0x0001'); % start ADC DMA
data_rx=read_data(session, N); % read data from the buffer to this PC over TCP/IP

figure;
plot(20*log10(abs(fftshift(fft(data_rx))))); % plot the RX data
%%
close_rfsoc(session); % close the session
