function [output] = close_rfsoc(session)
output=send_and_wait_for_response(session.command_socket, 'disconnect');

delete(session.command_socket);
delete(session.data_socket);
end

