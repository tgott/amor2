% for loading data saved by read_and_save_RAW_data --- I16s in real and imag separate files
directory='C:\_data_MV\';

tic
fid=fopen([directory 'data_real_I16.dat'],'rb');
data_real=fread(fid,'int16');
fclose(fid);

fid=fopen([directory 'data_imag_I16.dat'],'rb');
data_imag=fread(fid,'int16');
fclose(fid);

data_rx=data_real+1i*data_imag;
data_rx=data_rx/32768; % scale to +- 1
clear data_real data_imag
toc;