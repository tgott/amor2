% for loading data saved by read_and_save_RAW_data_to_disk --- I16s in real and imag interleaved in one file
filename='C:\_data_MV\data_I16_interleaved_123.dat';

tic
fid=fopen(filename,'rb');
data_rx=fread(fid,'int16');
fclose(fid);

data_rx=reshape(data_rx, 8, []); % samples are interleaved 8*I 8*Q 8*I 8*Q and so on
I=data_rx(:,1:2:end);
I=I(:);
Q=data_rx(:,2:2:end);
Q=Q(:);

data_rx=I+1i*Q;
data_rx=data_rx/32768; % scale to +- 1
clear I Q
toc;