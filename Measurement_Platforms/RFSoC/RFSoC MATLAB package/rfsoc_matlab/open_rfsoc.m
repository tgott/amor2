function [session] = open_rfsoc(ip)
session.data_socket=tcpclient(ip,8082,"Timeout",30);
session.command_socket=tcpclient(ip,8081,"Timeout",30);
pause(1); % we must wait until memory initialization finishes
end

