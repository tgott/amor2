% read_and_save_RAW_data(session, 1024*1024*256*28, 'C:\_data_MV');
% saves RAW I16 data into separate real and imag files
% use load_big_data_I16.m to read

function read_and_save_RAW_data(session, num_samples, filename)
if mod(num_samples, 8)
    error('Number of samples must be multiple of 8');
end

if exist('filename','var')
    write_file = 1;
    fid_real=fopen([filename '/data_real_I16.dat'],'wb');
    fid_imag=fopen([filename '/data_imag_I16.dat'],'wb');
else
    write_file = 0;
end

send_and_wait_for_response(session.command_socket, 'LocalMemTrigger 0 0 4096 0x0001'); % start ADC DMA (???)
write(session.data_socket,  uint8(['ReadDataFromMemory 0 0 ' num2str(num_samples*4) ' 1' 13 10]));
%pause(0.5);

tic;
bytes_to_read=num_samples*4;
bytes_per_read=1024*1024*16;
while (bytes_to_read>=0)
    if (bytes_to_read>bytes_per_read)
        data=read(session.data_socket, bytes_per_read); 
    else
        data=read(session.data_socket, bytes_to_read); 
    end
    
    if write_file == 1
      data=typecast(data, 'int16'); % samples are in I16
      data=reshape(data, 8, []); % samples are interleaved 8*I 8*Q 8*I 8*Q and so on
       I=data(:,1:2:end);
       I=I(:);
       Q=data(:,2:2:end);
       Q=Q(:);
       fwrite(fid_real,I,'int16');
       fwrite(fid_imag,Q,'int16');
    end
    
    fprintf("%d%% complete.\n", round((num_samples*4-bytes_to_read)/(num_samples*4)*100));
    bytes_to_read=bytes_to_read-bytes_per_read;
    
end
data=read(session.data_socket, 22); %% 22 -> \r\n + ReadDataFromMemory + \r\n
test=char(data)
b=toc;
fprintf('Transfer speed: %f MiB/s. Transfer size: %f MiB. Transfer time: %f seconds.\r\n', num_samples*4/b/1024/1024, num_samples*4/1024/1024 , b);

if write_file == 1
    fclose(fid_real);
    fclose(fid_imag);
end
% data=read(session.data_socket, num_samples*4+22); %% 22 -> \r\n + ReadDataFromMemory + \r\n
% 
% data=data(1:num_samples*4); % strip the \r\n + ReadDataFromMemory + \r\n part
% data=typecast(data, 'int16'); % samples are in I16
% data=reshape(data, 8, []); % samples are interleaved 8*I 8*Q 8*I 8*Q and so on
% I=data(:,1:2:end);
% I=I(:);
% Q=data(:,2:2:end);
% Q=Q(:);

end

