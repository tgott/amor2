% read_and_save_RAW_data_to_disk(session, 1024*1024*256*28, 0);
% saves RAW I16 data into one one file on the SSD of the RFSoC
% use load_big_data_interleaved_I16.m to read

function read_and_save_RAW_data_to_disk(session, num_samples, filename)
if mod(num_samples, 8)
    error('Number of samples must be multiple of 8');
end

send_and_wait_for_response(session.command_socket, 'LocalMemTrigger 0 0 4096 0x0001'); % start ADC DMA (???)
write(session.data_socket,  uint8(['ReadDataFromMemoryWriteToDisk 0 0 ' num2str(num_samples*4) ' 1 ' num2str(filename) 13 10]));
%pause(0.5);

progress_max=ceil(num_samples*4/(1024*1024*32));
for i=1:progress_max
    read(session.data_socket, 1);
    fprintf("%d%% write complete.\n", round(i/progress_max*100));
    if i==1
        tic
    end
end


data=read(session.data_socket, 31); %% 33 -> ReadDataFromMemoryWriteToDisk + \r\n
test=char(data)

b=toc;
fprintf('Transfer speed: %f MiB/s. Transfer size: %f MiB. Transfer time: %f seconds.\r\n', num_samples*4/b/1024/1024, num_samples*4/1024/1024 , b);

end

