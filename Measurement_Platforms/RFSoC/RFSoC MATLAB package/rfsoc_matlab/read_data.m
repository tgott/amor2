function [data, I, Q, data_raw] = read_data(session, num_samples)
if mod(num_samples, 8)
    error('Number of samples must be multiple of 8');
end

write(session.data_socket,  uint8(['ReadDataFromMemory 0 0 ' num2str(num_samples*4) ' 1' 13 10]));
pause(0.5);
data=read(session.data_socket, num_samples*4+22); %% 22 -> \r\n + ReadDataFromMemory + \r\n

data=data(1:num_samples*4); % strip the \r\n + ReadDataFromMemory + \r\n part
data_raw=data;
data=typecast(data, 'int16'); % samples are in I16
data=reshape(data, 8, []); % samples are interleaved 8*I 8*Q 8*I 8*Q and so on
I=data(:,1:2:end);
I=I(:);
Q=data(:,2:2:end);
Q=Q(:);

data=double(I)+double(Q)*1i; % create complex signal
data=data/32768; % scale to +- 1
end

