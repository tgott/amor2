function [output] = send_and_wait_for_response(socket, data)
for response_length=1:numel(data)
    if (data(response_length)==' ')
        break
    end
end

response_length=response_length+1; % count with \r\n
write(socket,  uint8([data 13 10]));
output=char(read(socket, response_length));
if (socket.BytesAvailable ~= 0)
    output=[output char(read(socket))];
end

end

