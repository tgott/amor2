function [out] = write_data(session, data)
if mod(numel(data), 8)
    error('Number of samples must be multiple of 8');
end
data=data(:).'; % ensure that we have row vector

I=int16(32767*real(data)); % scale to int16 range
Q=int16(32767*imag(data)); % scale to int16 range

data_interleaved=[I; Q]; % DAC data are interleaved sample by sample e.g. I Q I Q I Q and so on
data_interleaved=data_interleaved(:)';

data=char(typecast(data_interleaved, 'uint8'));
out=send_and_wait_for_response(session.data_socket, ['WriteDataToMemory 1 2 ' num2str(numel(data)) ' 1' 13 10 data]);
end

