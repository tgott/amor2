clc
clearvars
close all

b=bladeRF_AMOR();
%%
b.rx.frequency  = 1089e6 - 15e6; % 1089-15 MHz, the signal is then shifted + 15 MHz by DDS in FPGA
b.rx.channel='BLADERF_CHANNEL_RX1';
b.rx.samplerate = 61.44e6;
b.rx.bandwidth  = 50e6;
b.rx.agc='MANUAL';
b.rx.gain=50;

N=512; % number of samples (FFT bins)
x=(0:N-1)-N/2; % x-scale in samples from -N/2 to N/2-1
x=x/N*b.rx.samplerate/2/1e6; % x-scale in frequency relative to center frequency (1089 MHz) in MHz
% x=x + 1089; % x-scale in absolute frequency in MHz

b.rx.config.num_buffers   = 2;
b.rx.config.buffer_size   = 1024;
b.rx.config.num_transfers = 1;
b.rx.config.timeout_ms    = 10000;


%% this block does the actual data acquisition --- it is recomended to run the first two sections to initialize the HW and then run this section for every measurement (ctrl-enter)
fig=1;

b.rx.start();
h=figure(fig);
close(h);
h=figure(fig);
h.Position=[500, 200, 800, 800];

while (1) % this will repeat until keypress
cut=0; % if this is nonzero, it will cut first "cut" frames --- for debugging

[samples_all, timestamp_out, actual_count, overrun] = b.receive_raw(N*(cut+1));
samples=samples_all(2*N*cut+1:2*N*(cut+1));


subplot(2,1,1);

peak_spectrum=samples(1:2:end);
quasi_peak_spectrum=samples(2:2:end);

hold off
plot(x,20*log10(fftshift (peak_spectrum))); 
hold on
plot(x,20*log10(fftshift (quasi_peak_spectrum))); 
grid on;
xlim([x(1) x(end)]);
ylim([-0 90]);

xlabel('frequency [MHz]');
ylabel('(quasi) peak power [dB])');
% legend('peak', 'quasi-peak'); % this slows the rendering unnecessarily


subplot(2,1,2);
plot(x,20*log10(fftshift(peak_spectrum)) - 20*log10(fftshift(quasi_peak_spectrum)) ); 
grid on;
xlim([x(1) x(end)]);
ylim([0 50]);

xlabel('frequency [MHz]');
ylabel('difference between the peak and quasi-peak power [dB])');

20*log10(peak_spectrum(1)) - 20*log10(quasi_peak_spectrum(1)) % display the difference between peak and quasi-peak power at 0 MHz
figure(fig);
drawnow
     isKeyPressed = ~isempty(get(h,'CurrentCharacter'));
     if isKeyPressed
         break
     end
end
%  sum(abs(samples)) % for debugging --- make sure that some frames (after reset) are full of zeros
b.rx.stop();
%%

clear b % close the session